package com.freightos.vendingmachine.controller;

import com.freightos.vendingmachine.businesslogic.Slot;
import com.freightos.vendingmachine.businesslogic.SnackSlots;
import com.freightos.vendingmachine.view.SnackSlotsView;

public class SnackSlotsController {

	private SnackSlots snackSlotModel;
	private SnackSlotsView snackSlotView;
	private static String id;
	private final static int SLOT_SIZE = 10;

	public SnackSlotsController(SnackSlots snackSlotModel, SnackSlotsView snackSlotView) {
		super();
		this.snackSlotModel = snackSlotModel;
		this.snackSlotView = snackSlotView;
	}

	public String generateUniqueId() {
		return id;

	}

	public void addSlots() {
		if (snackSlotModel.getSlots().size() < snackSlotModel.getSize())
			snackSlotModel.setSlot(new Slot(generateUniqueId(), SLOT_SIZE));

	}

	public void updateView() {
		snackSlotView.printSnackSlotDetails(snackSlotModel.getSlots(), snackSlotModel.getRows(),
				snackSlotModel.getColumns(), snackSlotModel.getSize());
	}

}

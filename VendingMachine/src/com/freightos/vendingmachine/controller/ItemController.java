package com.freightos.vendingmachine.controller;

import com.freightos.vendingmachine.businesslogic.Item;
import com.freightos.vendingmachine.view.ItemView;

public class ItemController {

	private Item itemModel;
	private ItemView itemView;

	public void setItemName(String name) {
		itemModel.setName(name);
	}

	public String getItemName() {
		return itemModel.getName();
	}

	public void setItemPrice(int price) {
		itemModel.setPrice(price);
	}

	public float getItemPrice() {
		return itemModel.getPrice();
	}

	public void updateView() {
		itemView.printItemDetails(getItemName(), getItemPrice());
	}

	public ItemController(Item itemModel, ItemView itemView) {
		super();
		this.itemModel = itemModel;
		this.itemView = itemView;
	}

}

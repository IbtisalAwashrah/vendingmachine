package com.freightos.vendingmachine.controller;

import java.util.List;

import com.freightos.vendingmachine.businesslogic.Item;
import com.freightos.vendingmachine.businesslogic.Slot;
import com.freightos.vendingmachine.view.SlotView;

public class SlotController {
	private Slot slotModel;
	private SlotView slotView;

	public SlotController(Slot slotModel, SlotView slotView) {
		super();
		this.slotModel = slotModel;
		this.slotView = slotView;
	}

	public void addItem(String name, float price) {
		slotModel.setItems(new Item(name, price));
	}

	public boolean removeItem(String name, float price) {
		return slotModel.getItems().remove(new Item(name, price));

	}

	public boolean isEmpty() {
		return slotModel.getItems().isEmpty();
	}

	public boolean isFull() {
		if (slotModel.getItems().size() >= slotModel.getCapacity())
			return true;
		return false;
	}

	public String getId() {
		return slotModel.getId();
	}

	public void setId(String id) {
		slotModel.setId(id);
	}

	public int getCapacity() {
		return slotModel.getCapacity();
	}

	public void setCapacity(int capacity) {
		slotModel.setCapacity(capacity);
	}

	public List<Item> getItems() {
		return slotModel.getItems();
	}

	public int getQuantity() {
		return slotModel.getQuantity();
	}

	public void updateView() {
		slotView.printSlotDetails(getItems(), getId(), getCapacity(), getQuantity());
	}

}

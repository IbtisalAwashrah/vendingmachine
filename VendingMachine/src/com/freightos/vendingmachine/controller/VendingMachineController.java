package com.freightos.vendingmachine.controller;

import com.freightos.vendingmachine.businesslogic.VendingMachine;
import com.freightos.vendingmachine.businesslogic.money.MoneySlot;

/**
 * Factory Design Pattern
 * */
public interface VendingMachineController {

	public void selectItem(int itemId);

	public float getPrice();

	public double getPriceOfSelectedItem(String itemId);

	public boolean isItemAvailable(int itemId);

	public void updateTotalPaidMoney(MoneySlot money);

	public void userCanceledTheOrder();

	public void makeOrder();

	public void insertMoney(MoneySlot money);

	public double getAccumulatedAmountOfMoney();

	public double getChargeAmount(VendingMachine vendingMachine);

	public Order completeOrder(VendingMachine vendingMachine);

	public void decreaseBalance();

}

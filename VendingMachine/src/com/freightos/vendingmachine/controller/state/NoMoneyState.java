package com.freightos.vendingmachine.controller.state;

public class NoMoneyState implements VendingMachineState {

	@Override
	public void selectProductAndInsertMoney(int productId) {
		System.out.println(productId + " has been selected...");

	}

	@Override
	public void dispenseProduct() {
		System.out.println(
				"Vending Machine cannot dispense product because money is not "
				+ "inserted and product is not selected...");
	}

}

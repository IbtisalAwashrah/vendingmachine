package com.freightos.vendingmachine.controller.state;

public interface VendingMachineState {
	 public void selectProductAndInsertMoney(int productId);
	 public void dispenseProduct();
}

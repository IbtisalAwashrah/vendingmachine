package com.freightos.vendingmachine.controller;

import com.freightos.vendingmachine.businesslogic.Slot;
import com.freightos.vendingmachine.businesslogic.VendingMachine;
import com.freightos.vendingmachine.businesslogic.money.MoneySlot;
import com.freightos.vendingmachine.controller.state.HasMoneyState;
import com.freightos.vendingmachine.controller.state.NoMoneyState;
import com.freightos.vendingmachine.controller.state.VendingMachineState;

public class VendingMachineControllerImpl implements VendingMachineController, VendingMachineState {

	private VendingMachineState vendingMachineState;
	private VendingMachine machine;

	public VendingMachineControllerImpl(VendingMachine vendingMachine) {
		vendingMachineState = new NoMoneyState();
		machine = vendingMachine;
	}

	public void selectItem() {
		double price = getPriceOfSelectedItem(machine.getPad().getValue());
		if (price != 0)
			machine.getPad().setValue("The Item Has No price Not Selected");
		else
			machine.getPad().setValue("The Item Was Selected and The Price is" + price);
	}

	public VendingMachineState getVendingMachineState() {
		return vendingMachineState;
	}

	public void setVendingMachineState(VendingMachineState vendingMachineState) {
		this.vendingMachineState = vendingMachineState;
	}

	@Override
	public void selectProductAndInsertMoney(int productId) {
		vendingMachineState.selectProductAndInsertMoney(productId);
		VendingMachineState hasMoneyState = new HasMoneyState();
		/*
		 * Money has been inserted so vending Machine changed the internal state to
		 * 'hasMoneyState'
		 */

		if (vendingMachineState instanceof NoMoneyState) {
			setVendingMachineState(hasMoneyState);
			System.out.println(
					"VendingMachine internal state has been moved to : " + vendingMachineState.getClass().getName());
		}

	}

	@Override
	public void dispenseProduct() {
		VendingMachineState noMoenyState = new NoMoneyState();
		vendingMachineState.dispenseProduct();

		/*
		 * Product has been dispensed so vending Machine changed the internal state to
		 * 'NoMoneyState'
		 */
		if (vendingMachineState instanceof HasMoneyState) {
			setVendingMachineState(noMoenyState);
			System.out.println(
					"VendingMachine internal state has been moved to : " + vendingMachineState.getClass().getName());
		}

	}

	@Override
	public void selectItem(int itemId) {
		// TODO Auto-generated method stub

	}

	@Override
	public float getPrice() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getPriceOfSelectedItem(String itemId) {
		int price = 0;
		for (int i = 0; i < machine.getSlots().getSlots().size(); i++) {
			if (itemId.equals(machine.getSlots().getSlots().get(i).getId()))
				price = (int) machine.getSlots().getSlots().get(i).getItems().get(0).getPrice();

		}
		return price;
	}

	@Override
	public boolean isItemAvailable(int itemId) {
		for (int i = 0; i < machine.getSlots().getSize(); i++)
			if (machine.getSlots().getSlots().get(i).getId().equals(itemId))
				return true;
		return false;
	}

	@Override
	public void updateTotalPaidMoney(MoneySlot money) {
		machine.getMoney().setValue(money.getValue());

	}

	@Override
	public void userCanceledTheOrder() {
		machine.reset();
	}

	@Override
	public void makeOrder() {
		// TODO Auto-generated method stub

	}

	@Override
	public void insertMoney(MoneySlot money) {
		// TODO Auto-generated method stub

	}

	@Override
	public double getAccumulatedAmountOfMoney() {
		return machine.getMoney().getBalance();
	}

	@Override
	public double getChargeAmount(VendingMachine vendingMachine) {
		return machine.getMoney().getValue();
	}

	@Override
	public Order completeOrder(VendingMachine vendingMachine) {
		Order order = new Order();

		Slot item = vendingMachine.getSlots().getSlots().get(0);

		order.setItem(item.getItems().get(0));
		item.setQuantity(item.getQuantity() - 1);

		vendingMachine.reset();

		return order;
	}

	public void decreaseBalance() {
		machine.getMoney().setValue(getPrice());

	}

}

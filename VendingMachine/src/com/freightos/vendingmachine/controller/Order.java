package com.freightos.vendingmachine.controller;

import java.util.List;

import com.freightos.vendingmachine.businesslogic.Item;
import com.freightos.vendingmachine.businesslogic.money.MoneySlot;


/**
 * This class responsibility to manage orders
 * */
public class Order {

	private double balance;
	private List<MoneySlot> changes;
	private Item item;

	public Order(double balance, List<MoneySlot> changes) {
		this.balance = balance;
		this.changes = changes;
	}

	public Order() {
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public List<MoneySlot> getChanges() {
		return changes;
	}

	public void setChanges(List<MoneySlot> changes) {
		this.changes = changes;
	}

	public void resetTheOrder() {
		this.balance = 0;
		this.changes = null;
		this.item = null;
	}
}

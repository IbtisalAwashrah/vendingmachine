package com.freightos.vendingmachine.model;

import java.util.ArrayList;

import com.freightos.vendingmachine.businesslogic.Slot;
import com.freightos.vendingmachine.businesslogic.SnackSlots;

/**
 * TODO Get from DB
 * 
 **/
public class SnackSlotsModel {
	public SnackSlots retriveSnackSlotsFromDatabase() {
		ArrayList<Slot> slots = new ArrayList<>();
		slots.add(new SlotModel().retriveSlotFromDatabase());
		return new SnackSlots(10, 10 , 10 , slots);
	}
}

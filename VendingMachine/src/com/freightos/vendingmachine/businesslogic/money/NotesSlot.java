package com.freightos.vendingmachine.businesslogic.money;

public class NotesSlot extends MoneySlot {
	private static final int TWENTY_DOLLARS = 20;
	private static final int FIFTY_DOLLARS = 50;

	private int denomination;

	private NotesSlot(int denomination) {
		this.denomination = denomination;
	}

	public int getDenomination() {
		return denomination;
	}

	@Override
	public double getValue() {
		return denomination;
	}

	@Override
	public void setValue(double value) {
		MoneySlot.balance += value;

	}

}

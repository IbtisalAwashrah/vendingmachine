package com.freightos.vendingmachine.businesslogic.money;

public class CoinSlot extends MoneySlot {
	private static final int TEEN_CENTS = 10;
	private static final int TWENTY_CENTS = 20;
	private static final int FIFTY_CENTS = 50;
	private static final int ONE_DOLLARS = 100;

	private int denomination;

	private CoinSlot(int denomination) {
		this.denomination = denomination;
	}

	public double getDenomination() {
		switch (denomination) {
		case TEEN_CENTS:
			return 0.1;
		case TWENTY_CENTS:
			return 0.2;
		case FIFTY_CENTS:
			return 0.5;
		case ONE_DOLLARS:
			return 1;
		}
		return 0;
	}

	@Override
	public double getValue() {
		return getDenomination() / 10;
	}

	@Override
	public void setValue(double value) {
		MoneySlot.balance += value;

	}

}

package com.freightos.vendingmachine.businesslogic.money;

/**
 * Money have 3 types in our case 
 * Strong is a relation ship
 * */
public abstract class MoneySlot {

	static double balance = 0;

	public abstract double getValue();

	public abstract void setValue(double value);
	public double  getBalance() {
		return balance;
	}

}

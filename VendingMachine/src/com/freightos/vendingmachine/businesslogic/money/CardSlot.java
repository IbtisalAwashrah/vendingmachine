package com.freightos.vendingmachine.businesslogic.money;

public class CardSlot extends MoneySlot {
	private String userName;
	private String number;
	private int csvNumber;
	private int checkedValue;

	CardSlot(String userName, String number, int csvNumber) {
		this.userName = userName;
		this.number = number;
		this.csvNumber = csvNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String vumber) {
		this.number = vumber;
	}

	public int getCsvNumber() {
		return csvNumber;
	}

	public void setCsvNumber(int csvNumber) {
		this.csvNumber = csvNumber;
	}

	@Override
	public double getValue() {
		return checkedValue;
	}

	@Override
	public void setValue(double value) {
		MoneySlot.balance += value;

	}

}

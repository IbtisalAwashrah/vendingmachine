package com.freightos.vendingmachine.businesslogic;

/**
 * KeyPad properties
 * */
public class KeyPad {

	private String value;

	public String getValue() {

		return value;
	}

	public void setValue(String value) {
		this.value = value;

	}

}

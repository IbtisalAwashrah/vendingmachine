package com.freightos.vendingmachine.businesslogic;

import com.freightos.vendingmachine.businesslogic.money.MoneySlot;

public class VendingMachine {

	
	private SnackSlots slots;
	private KeyPad pad;
	private MoneySlot money;

	public SnackSlots getSlots() {
		return slots;
	}

	public void setSlots(SnackSlots slots) {
		this.slots = slots;
	}

	public KeyPad getPad() {
		return pad;
	}

	public void setPad(KeyPad pad) {
		this.pad = pad;
	}

	public MoneySlot getMoney() {
		return money;
	}

	public void setMoney(MoneySlot money) {
		this.money = money;
	}

	public VendingMachine(SnackSlots slots, KeyPad pad, MoneySlot money) {
		super();
		this.slots = slots;
		this.pad = pad;
		this.money = money;
	}

	public void reset() {
		this.money.setValue(0);
		
	}

}

package com.freightos.vendingmachine.businesslogic;

import java.util.ArrayList;
import java.util.List;

/**
 * This class has a properties of all slots
 * **/
public class SnackSlots {
	private int rows;
	private int columns;
	private int size;
	private List<Slot> slots;

	public SnackSlots(int rows, int columns, int size, List<Slot> slots) {
		super();
		this.rows = rows;
		this.columns = columns;
		this.size = size;
		this.slots = new ArrayList<Slot>();
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = Math.multiplyExact(getRows(), getColumns());
	}

	public List<Slot> getSlots() {
		return slots;
	}

	public void setSlot(Slot slot) {
		this.slots.add(slot);
	}

}

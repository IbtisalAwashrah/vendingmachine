package com.freightos.vendingmachine.businesslogic;

import java.util.ArrayList;
import java.util.List;
/**
 * Each slot has a manyItems of same type 
 * **/
public class Slot {
	private List<Item> items;
	private String id; // ex:A1 
	private int capacity;
	private int quantity;

	public Slot(String id, int capacity) {
		super();
		this.items = new ArrayList<Item>();
		this.id = id;
		this.capacity = capacity;
		this.quantity = 0;
	}

	public Slot(String id, int capacity, List<Item> items) {
		super();
		this.items = items;
		this.id = id;
		this.capacity = capacity;
		this.quantity = 0;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public List<Item> getItems() {
		return items;
	}

	public int getQuantity() {
		this.quantity = items.size();
		return quantity;
	}

	public void setItems(Item item) {
		this.items.add(item);
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Slot [items=" + items + ", id=" + id + ", capacity=" + capacity + ", quantity=" + quantity + "]";
	}

}

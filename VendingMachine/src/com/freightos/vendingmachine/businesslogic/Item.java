/**
 * 
 */
package com.freightos.vendingmachine.businesslogic;

/**
 * @author Ibtisal Awashrah
 * 
 *         This class defined the main properties which each item has such as
 *         snacks items, drinks, etc
 */
public class Item {

	private String name;
	private float price;

	public Item(String name, float price) {
		super();
		this.name = name;
		this.price = price;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Item [name=" + name + ", price=" + price + "]";
	}

}

package com.freightos.vendingmachine.view;

import java.util.List;

import com.freightos.vendingmachine.businesslogic.Slot;

public class SnackSlotsView {
	public void printSnackSlotDetails(List<Slot> slots, int raw, int column, int size) {
		System.out.println(slots);
		for (int i = 0; i < slots.size(); i++)
			System.out.println("Items: " + slots.get(i).toString());
		System.out.println("Slot NO of raws: " + raw);
		System.out.println("Slot NO of columns: " + column);
		System.out.println("Slot size: " + size);
	}

}

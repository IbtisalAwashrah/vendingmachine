package com.freightos.vendingmachine.view;

import java.util.Scanner;



public class KeyPadView {
	private Scanner input;

	public void printOnScreen (String value) {
		System.out.println(value);
	}
	
	public String getFromKeys () {
		input = new Scanner(System.in);
		return input.nextLine();
		
	}

}

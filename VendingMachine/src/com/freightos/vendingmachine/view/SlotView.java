package com.freightos.vendingmachine.view;

import java.util.List;

import com.freightos.vendingmachine.businesslogic.Item;

public class SlotView {
	

	public void printSlotDetails(List<Item> items, String id, int capacity, int quantity) {
		for (int i = 0; i < items.size(); i++)
			System.out.println("Items: " + items.get(i).toString());
		System.out.println("Slot id: " + id);
		System.out.println("Slot Capacity: " + capacity);
		System.out.println("Slot Quantity: " + quantity);		
	}

}
